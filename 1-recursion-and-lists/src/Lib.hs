{-|
    Module      : Lib
    Description : Checkpoint voor V2DEP: recursie en lijsten
    Copyright   : (c) Brian van de Bijl, 2020
    License     : BSD3
    Maintainer  : nick.roumimper@hu.nl

    In dit practicum oefenen we met het schrijven van simpele functies in Haskell.
    Specifiek leren we hoe je recursie en pattern matching kunt gebruiken om een functie op te bouwen.
    LET OP: Hoewel al deze functies makkelijker kunnen worden geschreven met hogere-orde functies,
    is het hier nog niet de bedoeling om die te gebruiken.
    Hogere-orde functies behandelen we verderop in het vak; voor alle volgende practica mag je deze
    wel gebruiken.
    In het onderstaande commentaar betekent het symbool ~> "geeft resultaat terug";
    bijvoorbeeld, 3 + 2 ~> 5 betekent "het uitvoeren van 3 + 2 geeft het resultaat 5 terug".
-}

module Lib
    ( ex1, ex2, ex3, ex4, ex5, ex6, ex7
    ) where

-- TODO: Schrijf en documenteer de functie ex1, die de som van een lijst getallen berekent.
-- Voorbeeld: ex1 [3,1,4,1,5] ~> 14

-- | Een functie die de som van een lijst getallen berekent
ex1 :: [Int] -> Int
ex1 [] = 0 -- als leeg return 0
ex1 list = head list + ex1 (tail list) -- als niet leeg wordt het eerste element van de lijst verkregen met head list, en de rest van de lijst wordt met tail list. Vervolgens wordt het eerste element opgeteld bij het resultaat van een recursieve oproep van de functie.

-- TODO: Schrijf en documenteer de functie ex2, die alle elementen van een lijst met 1 ophoogt.
-- Voorbeeld: ex2 [3,1,4,1,5] ~> [4,2,5,2,6]

--   Als de invoerlijst leeg is, retourneert `ex2` een lege lijst.
--   In het andere geval verhoogt `ex2` elk element in de invoerlijst met 1 en geeft een nieuwe lijst terug
--   waarin de elementen met 1 zijn verhoogd.
ex2 :: [Int] -> [Int]
ex2 [] = [] -- als leeg return lege lijst
ex2 lst = (head lst + 1) : (ex2(tail lst)) -- 

-- TODO: Schrijf en documenteer de functie ex3, die alle elementen van een lijst met -1 vermenigvuldigt.
-- Voorbeeld: ex3 [3,1,4,1,5] ~> [-3,-1,-4,-1,-5]

--   Als de invoerlijst leeg is, retourneert `ex3` een lege lijst.
--   In het andere geval vermenigvuldigt `ex3` elk element in de invoerlijst met -1 en geeft een nieuwe lijst terug
--   waarin de elementen met -1 zijn vermenigvuldigd.
ex3 :: [Int] -> [Int]
ex3 [] = []
ex3 lst = (head lst * (-1)) : (ex3 (tail lst))

-- TODO: Schrijf en documenteer de functie ex4, die twee lijsten aan elkaar plakt.
-- Voorbeeld: ex4 [3,1,4] [1,5] ~> [3,1,4,1,5]
-- Maak hierbij geen gebruik van de standaard-functies, maar los het probleem zelf met (expliciete) recursie op. 
-- Hint: je hoeft maar door een van beide lijsten heen te lopen met recursie.

--   Als de eerste invoerlijst leeg is, retourneert `ex4` de tweede invoerlijst.
--   Als de tweede invoerlijst leeg is, retourneert `ex4` de eerste invoerlijst.
--   In het andere geval voegt `ex4` de elementen van de eerste invoerlijst gevolgd door de elementen van de tweede invoerlijst samen.
ex4 :: [Int] -> [Int] -> [Int]
ex4 x [] = x
ex4 [] y = y 
ex4 x y = head x : ex4 (tail x) y

-- TODO: Schrijf en documenteer een functie, ex5, die twee lijsten van gelijke lengte paarsgewijs bij elkaar optelt.
-- Voorbeeld: ex5 [3,1,4] [1,5,9] ~> [4,6,13]

--   Als de eerste invoerlijst leeg is, retourneert `ex5` de tweede invoerlijst.
--   Als de tweede invoerlijst leeg is, retourneert `ex5` de eerste invoerlijst.
--   In het andere geval telt `ex5` de overeenkomstige elementen van de twee invoerlijsten bij elkaar op.
-- Voorbeeld:
-- >>> ex5 [1, 2, 3] [4, 5, 6]
-- [5, 7, 9]
ex5 :: [Int] -> [Int] -> [Int]
ex5 xs [] = xs 
ex5 [] ys = ys 
ex5 (x:xs) (y:ys) = (x + y) : ex5 xs ys

-- TODO: Schrijf en documenteer een functie, ex6, die twee lijsten van gelijke lengte paarsgewijs met elkaar vermenigvuldigt.
-- Voorbeeld: ex6 [3,1,4] [1,5,9] ~> [3,5,36] 

--   Als de eerste invoerlijst leeg is, retourneert `ex6` de tweede invoerlijst.
--   Als de tweede invoerlijst leeg is, retourneert `ex6` de eerste invoerlijst.
--   In het andere geval vermenigvuldigt `ex6` de overeenkomstige elementen van de twee invoerlijsten met elkaar.
-- Voorbeeld:
-- >>> ex6 [1, 2, 3] [4, 5, 6]
-- [4, 10, 18]
ex6 :: [Int] -> [Int] -> [Int]
ex6 xs [] = xs 
ex6 [] ys = ys 
ex6 (x:xs) (y:ys) = (x * y) : ex6 xs ys 

-- TODO: Schrijf en documenteer een functie, ex7, die de functies ex1 en ex6 combineert tot een functie die het inwendig product uitrekent.
-- Voorbeeld: ex7 [3,1,4] [1,5,9] geeft 3*1 + 1*5 + 4*9 = 44 terug als resultaat.

--   Deze functie maakt gebruik van de `ex6` functie om het product van de elementen te berekenen en vervolgens
--   de `ex1` functie om de lijst met producten om te zetten in een enkel getal.
ex7 :: [Int] -> [Int] -> Int
ex7 x y = ex1 $ ex6 x y
